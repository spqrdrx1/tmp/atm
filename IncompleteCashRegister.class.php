<?php
declare(strict_types = 1);

class IncompleteCashRegister {
	
	// warning: mock data
	public function getAvailableChange() {
		return array(1 => 25, 2 => 74, 5 => 14, 10 => 18, 20 => 0, 50 => 5, 100 => 30, 200 => 15, 500 => 8, 1000 => 11, 2000 => 8, 5000 => 5, 10000 => 2, 20000 => 0, 50000 => 0);
	}
	
	
	// Calculate what change to give back to customer
	// Try to give as little items as possible
	// throw Exception if the change to give back is not available
	// throw Exception if what is paid by customer is insufficient
	public function calculateChangeInstructions(int $totalPrice,int $paidByCustomer) {
		$changeInstructions = array (1 => 0, 2 => 0, 5 => 0, 10 => 0, 20 => 0, 50 => 0, 100 => 0, 200 => 0, 500 => 0, 1000 => 0, 2000 => 0, 5000 => 0, 10000 => 0, 20000 => 0, 50000 => 0);
		
		$changeToGive = $paidByCustomer - $totalPrice;
		
		if($changeToGive < 0) { throw new Exception("Insufficient amount paid"); }
		if($changeToGive == 0) { return $changeInstructions; }
		
		$availableChange = $this->getAvailableChange();
		$availableChangeTotal = 0;
		foreach($availableChange as $cashValue => $cashQuantity) { $availableChangeTotal += $cashValue * $cashQuantity; }
		if($changeToGive > $availableChangeTotal) { throw new Exception("Insufficient cash in register");}
		
		
		// Loop over cash types (by descending order of value)
		// give the maximum amount possible we need of this cash type
		// then deduct the amount already given back to what is left to give back
		$changeToGiveLeft = $changeToGive;
		krsort($availableChange);
		foreach($availableChange as $cashValue => $cashQuantity) {
	
			if($changeToGiveLeft >= $cashValue && $cashQuantity > 0) {
				$toGiveOfThis = intdiv($changeToGiveLeft, $cashValue);
				if($toGiveOfThis > $cashQuantity) { $toGiveOfThis = $cashQuantity; }
				$changeInstructions[$cashValue] = $toGiveOfThis;
				$changeToGiveLeft -= ($toGiveOfThis * $cashValue);
			}
		}
		return $changeInstructions;
	}
}
?>