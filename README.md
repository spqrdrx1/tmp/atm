# ATM CONSULTING - Test  Technique

## Rendu de  monnaie pour caisse automatique

Attendu: 

> Créer une classe qui permet de savoir comment rendre la monnaie sur une somme en
> tenant compte des contraintes de monnaie disponible (quelles pièces, quels billets).
> Il faut toujours chercher à rendre le moins de pièces/billets possibles, c’est à dire que s’il y a
> 200€ à rendre et qu’un billet de 200€ est disponible, votre code ne devra pas rendre 200
> pièces de 1€.

## Livrable: Classe IncompleteCashRegister

### Instructions

- Importer le fichier IncompleteCashRegister.class.php 
- Instancier la classe (cf. Exemples)
- Utiliser la méthode calculateChangeInstructions en lui donnant en paramètres le prix à payer ainsi que la monnaie fournie
- La classe renverra en retour un tableau associatif de la forme suivante:

clé -> valeur pièce/billets exprimée en centimes (peut être 0)

valeur -> quantitée à rendre.

### Attention

**cette classe utilise des données fixes pour le contenu actuel de la caisse**

### Exceptions

La méthode lève une exception dans les cas suivants:

- la monnaie fournie ne suffit pas à payer l'achat
- la somme à rendre n'est pas disponible en caisse

### Exemples



```
$icr = new IncompleteCashRegister();
print_r($icr->calculateChangeInstructions(14532, 20000))
```
renvoie le tableau associatif suivant

```
Array
(
    [1] => 1
    [2] => 1
    [5] => 1
    [10] => 1
    [20] => 0
    [50] => 1
    [100] => 0
    [200] => 2
    [500] => 0
    [1000] => 0
    [2000] => 0
    [5000] => 1
    [10000] => 0
    [20000] => 0
    [50000] => 0
)

```

---------------------------------
```
$icr = new IncompleteCashRegister();
print_r($icr->calculateChangeInstructions(100, 1000))

```

renvoie le tableau associatif suivant
```
Array
(
    [1] => 0
    [2] => 0
    [5] => 0
    [10] => 0
    [20] => 0
    [50] => 0
    [100] => 0
    [200] => 2
    [500] => 1
    [1000] => 0
    [2000] => 0
    [5000] => 0
    [10000] => 0
    [20000] => 0
    [50000] => 0
)

```
---------------------------------
```
$icr = new IncompleteCashRegister();
print_r($icr->calculateChangeInstructions(1000, 1000))
```

renvoie le tableau associatif suivant

```
Array
(
    [1] => 0
    [2] => 0
    [5] => 0
    [10] => 0
    [20] => 0
    [50] => 0
    [100] => 0
    [200] => 0
    [500] => 0
    [1000] => 0
    [2000] => 0
    [5000] => 0
    [10000] => 0
    [20000] => 0
    [50000] => 0
)
```

--------------------------------------------
```
$icr = new IncompleteCashRegister();
print_r($icr->calculateChangeInstructions(1000, 1))
```

lève l'exception suivante:


```
Fatal error: Uncaught Exception: Insufficient amount paid
```

-----------------------------------------------
```
$icr = new IncompleteCashRegister();
print_r($icr->calculateChangeInstructions(1, 100000000000000000))
```
lève l'exception suivante

```
Fatal error: Uncaught Exception: Insufficient cash in register
```











